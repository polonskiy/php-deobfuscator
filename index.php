<?php

$data = trim(@$_POST['data']);
if (stripos($data, 'eval') !== false) {
	$data = str_ireplace(array('<?php', '?>', '<?'), '', $data);
	$data = str_ireplace('eval', 'echo', $data);
	ob_start();
	@eval($data);
	$data = ob_get_clean();
}
$data = htmlspecialchars($data, ENT_QUOTES);

?>

<form action="" method="post">
	<textarea name="data" style="width: 700px; height: 400px;"><?= $data; ?></textarea>
	<br>
	<input type="submit" value="deobfuscate">
</form>
